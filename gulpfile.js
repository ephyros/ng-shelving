/*global -$ */
'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var gulpIf = require('gulp-if');
var postcss = require('gulp-postcss');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('autoprefixer-core');
var minifyHtml = require('gulp-minify-html');
var angularTemplatecache = require('gulp-angular-templatecache');
var csso = require('gulp-csso');
var rename = require('gulp-rename');
var ngAnnotate = require('gulp-ng-annotate');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var karma = require('karma').server;

var tmpPath = '.tmp';

gulp.task('partials', function () {

  return gulp.src([
    'scripts/**/*.html'
  ])
    .pipe(sourcemaps.init())
    .pipe(minifyHtml({
      empty: true,
      spare: true,
      quotes: true
    }))
    .pipe(angularTemplatecache('templateCacheHtml.js', {
      module: 'shelving'
    }))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(tmpPath + '/scripts/'));
});
gulp.task('scripts', ['partials'], function () {

  return gulp.src([
    'scripts/shelving.js',
    'scripts/sh-*.js',
    tmpPath + '/scripts/*.js'
  ])
    .pipe(sourcemaps.init({loadMaps: true}))
    .pipe(concat('shelving.js'))
    .pipe(ngAnnotate())
    .pipe(gulp.dest('js/'))
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(uglify())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('js/'));
});


gulp.task('styles', function () {
  return gulp.src('sass/*.{scss,sass}')
    .pipe(sourcemaps.init())
    .pipe(gulpIf(/\.sass$/, sass({indentedSyntax: true})))
    .pipe(gulpIf(/\.scss$/, sass()))
    .pipe(postcss([
      // more here https://github.com/postcss/postcss
      autoprefixer({browsers: ['last 2 versions', 'ie >= 9']})
    ]))
    .pipe(gulp.dest('css'))
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(csso())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('css'));
});

gulp.task('watch', ['styles', 'scripts'], function () {
  gulp.watch('sass/**/*.{sass,scss}', ['styles']);
  gulp.watch('scripts/**/*', ['scripts']);
});

// The development server (the recommended option for development)
gulp.task("default", ["watch"]);

gulp.task('test', function (done) {
  karma.start({
    configFile: __dirname + '/karma.conf.js',
    singleRun: true
  }, done);
});
gulp.task('build', ['styles', 'scripts']);
