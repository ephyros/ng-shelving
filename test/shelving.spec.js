'use strict';

describe('module definition', function () {

  it('Module "shelving" should exist', function () {
    expect(!!angular.module('shelving')).toBe(true);
  });
});
