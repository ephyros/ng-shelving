'use strict';

module.exports = function (config) {
  config.set({
    autoWatch: false,

    frameworks: ['jasmine'],

    browsers: ['PhantomJS'],

    files: [
      'bower_components/angular/angular.js',
      'bower_components/angular-mocks/angular-mocks.js',
      'scripts/shelving.js',
      '.tmp/scripts/templateCacheHtml.js',
      'scripts/sh-*.js',
      'test/*.js'
    ],

    plugins: [
      'karma-phantomjs-launcher',
      'karma-jasmine'
    ]
  });
};
