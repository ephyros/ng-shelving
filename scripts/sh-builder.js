'use strict';

angular.module('shelving')
  .directive('shBuilder', function () {

    function intersect(x1, l1, x2, l2) {
      var t;
      if (x1 > x2) {
        t = x1;
        x1 = x2;
        x2 = t;
        t = l1;
        l1 = l2;
        l2 = t;
      }
      if (x1 + l1 > x2) {
        return Math.min(x1 + l1, x2 + l2) - x2;
      }
      return 0;
    }

    return {
      templateUrl: 'sh-builder.html',
      transclude: true,
      scope: {
        selection: '=',
        rows: '=',
        items: '=',
        ratio: '=',
        width: '=',
        listWidth: '=',
        shelvigWidth: '='
      },

      controllerAs: 'shBuilder',
      controller: function ($scope) {
        var that = this;
        that.items = $scope.items;
        that.ratio = $scope.ratio;
        that.listWidth = $scope.listWidth;
        that.shelvigWidth = $scope.shelvigWidth;

        this.itemsById = {};
        $scope.$watch('items', function () {
          $scope.items.forEach(function (item) {
            that.itemsById[item.id] = item;
          });
        });
        this.rows = $scope.rows;
        this.selection = $scope.selection;
        this.shelvingElement = null;


        this.activeItem = null;

        this.onItemMouseDown = function (e, item) {
          e.preventDefault();
          e.stopPropagation();
          var x = e.offsetX || (e.pageX - e.currentTarget.offsetLeft);
          var y = e.offsetY || (e.pageY - e.currentTarget.offsetTop);

          this.activeItem = {
            id: item.id,
            height: item.height,
            offsetX: x,
            offsetY: y,
            top: e.currentTarget.offsetTop,
            left: e.currentTarget.offsetLeft,
            cell: null
          }
        };

        this.onSelectedMouseDown = function (e, selected) {
          e.preventDefault();
          e.stopPropagation();
          var x = e.offsetX || (e.pageX - e.currentTarget.offsetLeft);
          var y = e.offsetY || (e.pageY - e.currentTarget.offsetTop);

          var item = that.itemsById[selected.id];

          this.activeItem = {
            id: item.id,
            height: item.height,
            offsetX: x,
            offsetY: y,
            top: e.currentTarget.offsetTop,
            left: e.currentTarget.offsetLeft,
            cell: [selected.cellX, selected.cellY]
          }
        };

        this.onMouseUp = function (e) {
          this.activeItem = null;
        };

        this.onMouseLeave = function (e) {
          this.activeItem = null;
        };

        function isCellFree(i, j, h) {
          var isFree = true;
          var selection = that.selection[i];
          for (var jj = 0; jj < selection.length; jj++) {
            var item = selection[jj];
            var j1 = item.cellY;
            var j2 = j1 + that.itemsById[item.id].height;
            if (j >= j2) continue;
            if (j + h <= j1) continue;
            isFree = false;
            break;
          }

          return isFree;
        }

        function getIndexByCellY(i, cellY) {
          var selection = that.selection[i];
          for (var jj = 0; jj < selection.length; jj++) {
            var item = selection[jj];
            if (item.cellY == cellY) return jj;
          }
          return null;
        }

        function compareByCellY(a, b) {
          return -Math.abs(that.rows / 2 - a.cellY) + Math.abs(that.rows / 2 - b.cellY);
        }

        this.onMouseMove = function (e) {


          function removeCell() {
            var oldX = that.activeItem.cell[0];
            var oldY = that.activeItem.cell[1];
            var index = getIndexByCellY(oldX, oldY);
            that.selection[oldX].splice(index, 1);
            that.selection[oldX].sort(compareByCellY);
            that.activeItem.cell = null;
          }

          function addCell() {
            that.selection[cellX].push({
              id: that.activeItem.id,
              cellY: cellY,
              cellX: cellX
            });

            that.activeItem.cell = [cellX, cellY];
            that.selection[cellX].sort(compareByCellY);
          }

          if (this.activeItem !== null) {
            var t = this.activeItem.top = e.pageY - this.activeItem.offsetY;
            var l = this.activeItem.left = e.pageX - this.activeItem.offsetX;
            var w = that.shelvigWidth;
            var h = that.shelvigWidth * that.ratio * that.activeItem.height;
            var rect = that.shelvingElement[0].getBoundingClientRect();


            var x;
            var y;
            var s;


            var cellS = -1, cellX, cellY;

            if (that.activeItem.cell) {
              cellX = that.activeItem.cell[0];
              cellY = that.activeItem.cell[1];
              x = cellX * (that.shelvigWidth + that.shelvigWidth *.05);
              y = cellY * that.shelvigWidth * that.ratio;
              cellS = intersect(t, h, rect.top + y, h) * intersect(l, w, rect.left + x, w);
            }

            for (var i = 0; i < that.selection.length; i++) {
              for (var j = 0; j < that.rows - that.activeItem.height + 1; j++) {
                x = i * (that.shelvigWidth + that.shelvigWidth *.05);
                y = j * that.shelvigWidth * that.ratio;
                s = intersect(t, h, rect.top + y, h) * intersect(l, w, rect.left + x, w);
                if (s > cellS) {
                  if (isCellFree(i, j, that.activeItem.height)) {
                    cellX = i;
                    cellY = j;
                    cellS = s;
                  }
                }
              }
            }

            if (cellS * 3 > w * h) {
              if (that.activeItem.cell) {
                i = that.activeItem.cell[0];
                j = that.activeItem.cell[1];

                if (i != cellX || j != cellY) {
                  //move cell
                  removeCell();
                  addCell();
                }
              } else {
                // add cell
                addCell();
              }
            } else {
              // remove from selection
              if (that.activeItem.cell) {
                removeCell();
              }
            }
          }

        };


      }
    }
  });
