'use strict';

angular.module('shelving')
  .directive('shItems', function () {
    return {
      require: '^shBuilder',
      templateUrl: 'sh-items.html',
      scope:{},
      link: function ($scope, element, attrs, shBuilder) {
        $scope.ratio = shBuilder.ratio;
        $scope.width = shBuilder.listWidth;
        $scope.items = shBuilder.items;
        $scope.onMouseDown = function($event, item){
          shBuilder.onItemMouseDown($event, item);
        }
      }
    }
  });
