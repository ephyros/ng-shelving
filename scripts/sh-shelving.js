'use strict';

angular.module('shelving')
  .directive('shShelving', function () {
    return {
      require: '^shBuilder',
      templateUrl: 'sh-shelving.html',
      scope: {},
      link: function ($scope, element, attrs, shBuilder) {
        $scope.ratio = shBuilder.ratio;
        $scope.width = shBuilder.shelvigWidth;
        $scope.itemsById = shBuilder.itemsById;
        $scope.selection = shBuilder.selection;
        $scope.rows = shBuilder.rows;

        shBuilder.shelvingElement = element;

        $scope.onMouseDown = shBuilder.onSelectedMouseDown.bind(shBuilder);
      }
    }
  });
