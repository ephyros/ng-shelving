'use strict';

angular.module('shelving', []);

'use strict';

angular.module('shelving')
  .directive('shBuilder', function () {

    function intersect(x1, l1, x2, l2) {
      var t;
      if (x1 > x2) {
        t = x1;
        x1 = x2;
        x2 = t;
        t = l1;
        l1 = l2;
        l2 = t;
      }
      if (x1 + l1 > x2) {
        return Math.min(x1 + l1, x2 + l2) - x2;
      }
      return 0;
    }

    return {
      templateUrl: 'sh-builder.html',
      transclude: true,
      scope: {
        selection: '=',
        rows: '=',
        items: '=',
        ratio: '=',
        width: '=',
        listWidth: '=',
        shelvigWidth: '='
      },

      controllerAs: 'shBuilder',
      controller: ["$scope", function ($scope) {
        var that = this;
        that.items = $scope.items;
        that.ratio = $scope.ratio;
        that.listWidth = $scope.listWidth;
        that.shelvigWidth = $scope.shelvigWidth;

        this.itemsById = {};
        $scope.$watch('items', function () {
          $scope.items.forEach(function (item) {
            that.itemsById[item.id] = item;
          });
        });
        this.rows = $scope.rows;
        this.selection = $scope.selection;
        this.shelvingElement = null;


        this.activeItem = null;

        this.onItemMouseDown = function (e, item) {
          e.preventDefault();
          e.stopPropagation();
          var x = e.offsetX || (e.pageX - e.currentTarget.offsetLeft);
          var y = e.offsetY || (e.pageY - e.currentTarget.offsetTop);

          this.activeItem = {
            id: item.id,
            height: item.height,
            offsetX: x,
            offsetY: y,
            top: e.currentTarget.offsetTop,
            left: e.currentTarget.offsetLeft,
            cell: null
          }
        };

        this.onSelectedMouseDown = function (e, selected) {
          e.preventDefault();
          e.stopPropagation();
          var x = e.offsetX || (e.pageX - e.currentTarget.offsetLeft);
          var y = e.offsetY || (e.pageY - e.currentTarget.offsetTop);

          var item = that.itemsById[selected.id];

          this.activeItem = {
            id: item.id,
            height: item.height,
            offsetX: x,
            offsetY: y,
            top: e.currentTarget.offsetTop,
            left: e.currentTarget.offsetLeft,
            cell: [selected.cellX, selected.cellY]
          }
        };

        this.onMouseUp = function (e) {
          this.activeItem = null;
        };

        this.onMouseLeave = function (e) {
          this.activeItem = null;
        };

        function isCellFree(i, j, h) {
          var isFree = true;
          var selection = that.selection[i];
          for (var jj = 0; jj < selection.length; jj++) {
            var item = selection[jj];
            var j1 = item.cellY;
            var j2 = j1 + that.itemsById[item.id].height;
            if (j >= j2) continue;
            if (j + h <= j1) continue;
            isFree = false;
            break;
          }

          return isFree;
        }

        function getIndexByCellY(i, cellY) {
          var selection = that.selection[i];
          for (var jj = 0; jj < selection.length; jj++) {
            var item = selection[jj];
            if (item.cellY == cellY) return jj;
          }
          return null;
        }

        function compareByCellY(a, b) {
          return -Math.abs(that.rows / 2 - a.cellY) + Math.abs(that.rows / 2 - b.cellY);
        }

        this.onMouseMove = function (e) {


          function removeCell() {
            var oldX = that.activeItem.cell[0];
            var oldY = that.activeItem.cell[1];
            var index = getIndexByCellY(oldX, oldY);
            that.selection[oldX].splice(index, 1);
            that.selection[oldX].sort(compareByCellY);
            that.activeItem.cell = null;
          }

          function addCell() {
            that.selection[cellX].push({
              id: that.activeItem.id,
              cellY: cellY,
              cellX: cellX
            });

            that.activeItem.cell = [cellX, cellY];
            that.selection[cellX].sort(compareByCellY);
          }

          if (this.activeItem !== null) {
            var t = this.activeItem.top = e.pageY - this.activeItem.offsetY;
            var l = this.activeItem.left = e.pageX - this.activeItem.offsetX;
            var w = that.shelvigWidth;
            var h = that.shelvigWidth * that.ratio * that.activeItem.height;
            var rect = that.shelvingElement[0].getBoundingClientRect();


            var x;
            var y;
            var s;


            var cellS = -1, cellX, cellY;

            if (that.activeItem.cell) {
              cellX = that.activeItem.cell[0];
              cellY = that.activeItem.cell[1];
              x = cellX * (that.shelvigWidth + that.shelvigWidth *.05);
              y = cellY * that.shelvigWidth * that.ratio;
              cellS = intersect(t, h, rect.top + y, h) * intersect(l, w, rect.left + x, w);
            }

            for (var i = 0; i < that.selection.length; i++) {
              for (var j = 0; j < that.rows - that.activeItem.height + 1; j++) {
                x = i * (that.shelvigWidth + that.shelvigWidth *.05);
                y = j * that.shelvigWidth * that.ratio;
                s = intersect(t, h, rect.top + y, h) * intersect(l, w, rect.left + x, w);
                if (s > cellS) {
                  if (isCellFree(i, j, that.activeItem.height)) {
                    cellX = i;
                    cellY = j;
                    cellS = s;
                  }
                }
              }
            }

            if (cellS * 3 > w * h) {
              if (that.activeItem.cell) {
                i = that.activeItem.cell[0];
                j = that.activeItem.cell[1];

                if (i != cellX || j != cellY) {
                  //move cell
                  removeCell();
                  addCell();
                }
              } else {
                // add cell
                addCell();
              }
            } else {
              // remove from selection
              if (that.activeItem.cell) {
                removeCell();
              }
            }
          }

        };


      }]
    }
  });

'use strict';

angular.module('shelving')
  .directive('shItems', function () {
    return {
      require: '^shBuilder',
      templateUrl: 'sh-items.html',
      scope:{},
      link: function ($scope, element, attrs, shBuilder) {
        $scope.ratio = shBuilder.ratio;
        $scope.width = shBuilder.listWidth;
        $scope.items = shBuilder.items;
        $scope.onMouseDown = function($event, item){
          shBuilder.onItemMouseDown($event, item);
        }
      }
    }
  });

'use strict';

angular.module('shelving')
  .directive('shShelving', function () {
    return {
      require: '^shBuilder',
      templateUrl: 'sh-shelving.html',
      scope: {},
      link: function ($scope, element, attrs, shBuilder) {
        $scope.ratio = shBuilder.ratio;
        $scope.width = shBuilder.shelvigWidth;
        $scope.itemsById = shBuilder.itemsById;
        $scope.selection = shBuilder.selection;
        $scope.rows = shBuilder.rows;

        shBuilder.shelvingElement = element;

        $scope.onMouseDown = shBuilder.onSelectedMouseDown.bind(shBuilder);
      }
    }
  });

angular.module("shelving").run(["$templateCache", function($templateCache) {$templateCache.put("sh-builder.html","<div ng-mousemove=\"shBuilder.onMouseMove($event)\" ng-mouseup=\"shBuilder.onMouseUp($event)\" ng-mouseleave=\"shBuilder.onMouseLeave($event)\"><ng-transclude></ng-transclude><div ng-if=\"shBuilder.activeItem && !shBuilder.activeItem.cell\"><div style=\"position: absolute;\" ng-style=\"{ width:width+\'px\', height:shBuilder.activeItem.height*width*ratio+\'px\', top:shBuilder.activeItem.top+\'px\', left:shBuilder.activeItem.left+\'px\' }\"><div style=\"width:100%; background-size:100%; background-repeat: no-repeat; position: absolute;\" ng-style=\"{ \'top\':shBuilder.itemsById[shBuilder.activeItem.id].imageTop+\'%\', \'height\':shBuilder.itemsById[shBuilder.activeItem.id].imageHeight+\'%\', \'background-image\': \'url(\'+shBuilder.itemsById[shBuilder.activeItem.id].image+\')\' }\"></div></div></div></div>");
$templateCache.put("sh-items.html","<div class=\"shelve-items\"><div ng-repeat=\"item in items\"><div style=\"margin-bottom: 5px; position: relative;\" ng-style=\"{\'height\': item.height*width*ratio+\'px\', width: width+\'px\'}\" ng-mousedown=\"onMouseDown($event, item)\"><div style=\"width:100%; background-size:100%; background-repeat: no-repeat; position: absolute;\" ng-style=\"{ \'top\':item.imageTop+\'%\', \'height\':item.imageHeight+\'%\', \'background-image\': \'url(\'+item.image+\')\' }\"></div></div></div></div>");
$templateCache.put("sh-shelving.html","<div class=\"shelves-wrap\"><div class=\"wall\" ng-style=\"{ height:width*ratio*rows + \'px\', width:width*0.05+\'px\' }\"><div style=\"height:100%; position: absolute; box-sizing: border-box;\" ng-style=\"{ \'left\':width*0.05+\'px\', \'width\':item.cellX*width*ratio/2+\'px\', \'border-style\':\'solid\', \'border-width\':\'\'+width*ratio/2+\'px \'+width*ratio/2+\'px \'+width*ratio/2+\'px \'+width*ratio/2+\'px\', \'border-color\':\'transparent transparent transparent #4A2F1E\' }\"></div></div><div style=\"height:100%; position: absolute; box-sizing: border-box; display: inline-block\" ng-style=\"{ \'height\':width*ratio*rows + \'px\', \'right\':width*0.05+\'px\', \'width\':item.cellX*width*ratio/2+\'px\', \'border-style\':\'solid\', \'border-width\':\'\'+width*ratio/2+\'px \'+width*ratio/2+\'px \'+width*ratio/2+\'px \'+width*ratio/2+\'px\', \'border-color\':\'transparent #4A2F1E transparent transparent\' }\"></div><div ng-repeat=\"column in selection\" style=\"display: inline-block\"><div ng-mouseup=\"onMouseUp($event)\" ng-mousemove=\"onMouseMove($event)\" style=\"display: inline-block\"><div style=\"position: relative;\" ng-style=\"{ height:width*ratio*rows + \'px\', width:width+\'px\'}\"><div ng-repeat=\"item in column\"><div style=\"width:100%; position:absolute;\" ng-style=\"{ height:itemsById[item.id].height*width*ratio+\'px\', top:item.cellY*width*ratio+\'px\' }\" ng-mousedown=\"onMouseDown($event, item)\"><div ng-if=\"item.cellY>rows/2\" style=\"width:100%; position: absolute; box-sizing: border-box;\" ng-style=\"{ \'top\':itemsById[item.id].imageTop+\'%\', \'margin-top\':\'-\'+width*ratio*(item.cellY-rows/2)/rows+\'px\', \'height\':width*ratio*(item.cellY-rows/2)/rows+\'px\', \'border-style\':\'solid\', \'border-width\':\'0px \'+item.cellX*width*ratio/2+\'px \'+width*ratio*(item.cellY-rows/2)/rows+\'px \'+(1-item.cellX)*width*ratio/2+\'px\', \'border-color\':\'transparent transparent \'+itemsById[item.id].color+\' transparent\' }\"></div><div style=\"width:100%; background-size:100%; background-repeat: no-repeat; position: absolute;\" ng-style=\"{ \'top\':itemsById[item.id].imageTop+\'%\', \'height\':itemsById[item.id].imageHeight+\'%\', \'background-image\': \'url(\'+itemsById[item.id].image+\')\' }\"></div><div ng-if=\"item.cellY+itemsById[item.id].height<rows/2\" style=\"width:100%; position: absolute; box-sizing: border-box;\" ng-style=\"{ \'top\':itemsById[item.id].imageTop+itemsById[item.id].imageHeight+\'%\', \'height\':width*ratio*(rows/2-item.cellY-itemsById[item.id].height)/rows+\'px\', \'border-style\':\'solid\', \'border-width\':\'\'+width*ratio*(rows/2-item.cellY-itemsById[item.id].height)/rows+\'px \'+item.cellX*width*ratio/2+\'px 0px \'+(1-item.cellX)*width*ratio/2+\'px\', \'border-color\':\'\'+itemsById[item.id].color+\' transparent transparent transparent\' }\"></div></div></div></div></div><div ng-if=\"!$last\" class=\"wall\" ng-style=\"{ height:width*ratio*rows + \'px\', width:width*0.05+\'px\'}\"></div></div><div class=\"wall\" ng-style=\"{ height:width*ratio*rows + \'px\', width:width*0.05+\'px\' }\"></div></div>");}]);
